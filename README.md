# NodeJS example for Docksal

A basic "Hello world" NodeJS app example.

## Features

- Uses and extends from Docksal's NodeJS stack (`DOCKSAL_STACK=node`)
- Example of stock `docksal/cli` image customization via [Dockerfile](.docksal/services/cli/Dockerfile) to install custom node version
- Live reload via `nodemod` 

## Prerequisites

[Docksal](https://docs.docksal.io)

## Usage

```bash
git clone https://gitlab.com/schaer/ext/docksal-boilerplate-nodejs.git <name>
cd <name>
fin init
```

Open [http://<name>.docksal](http://<name>.docksal) 
